
import 'dart:typed_data';

import 'package:http/http.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
final supabase = Supabase.instance.client;
Future<void> sign_out({
  required Function onResponse,
  required Function onError,
})async{
  try{
    await supabase.auth.signOut();
  }
  on AuthException catch(e){
    onError(e.message);
  }
  on Exception catch(e){
    onError("Something went wrong");
  }
}
Future<String> insertNewOrder(
    String address,
    String country,
    String phone,
    String others,
    String items,
    int weight,
    int worth,
    double delivery_charges,
    double instantDelivery,
    double tax,
    double sum_price,
    List<Map<String, String>> destinations)async{
  String orderId = await supabase.from("orders").insert(
      {
        "id_user": supabase.auth.currentUser!.id,
        "address": address,
        "country": country,
        "phone": phone,
        "others": others,
        "package_items": items,
        "weight_items": weight,
        "worth_items": worth,
        "delivery_charges": delivery_charges,
        "instant_delivery": instantDelivery,
        "tax_and_service_charges": tax,
        "sum_price": sum_price
      }
  ).select().single().then((value) => value["id"]);
  for (var i in destinations) {
    i["id_order"] = orderId;
    await supabase.from("destinations_details").insert(i);
  }
  return orderId;

}
Future<String> uploadAvatar(Uint8List bytes)async{
  return await supabase.storage.from("avatars").
  uploadBinary("${supabase.auth.currentUser!.id}.png", bytes,
      fileOptions: FileOptions(cacheControl: '3600', upsert: false));
}
Future<String> getAvatar()async{
  var url = 'https://uboklrrvwysdoztwfvuj.supabase.co/storage/v1/object/public/avatars/${supabase.auth.currentUser!.id}.png';
  final response = await get(
      Uri.parse(url)
  );
  if (response.statusCode == 200){
    return url;
  }else{
    return "https://uboklrrvwysdoztwfvuj.supabase.co/storage/v1/object/public/avatars/default.png";
  }
}
Future<void> subscribeOrder(String orderId, callback) async {
  supabase
      .channel("orders-status-changes")
      .onPostgresChanges(
      event: PostgresChangeEvent.update,
      schema: "public",
      table: "orders",
      filter: PostgresChangeFilter(
          type: PostgresChangeFilterType.eq,
          column: "id",
          value: orderId
      ),
      callback: (payload) {
        callback(payload.newRecord);
      }
  )
      .subscribe();
}
Future<Map<String, dynamic>> getUser() async{
  return await supabase.
  from("profiles").
  select().
  eq("id_user", supabase.auth.currentUser!.id).
  single();
}
Future<List<Map<String, dynamic>>> getDestinationDetailsOrder(id_order) async{
  return await supabase.
  from("destination_details").
  select().
  eq("id_order", id_order);
}
Future<List<Map<String, dynamic>>> getTransactions() async{
  return await supabase.
  from("transactions").
  select().
  eq("id_user", supabase.auth.currentUser!.id);
}
Future<Map<String, dynamic>> getOrder() async{
  return await supabase.
  from("orders").
  select().
  eq("id_user", supabase.auth.currentUser!.id).
  single();
}
Future<List<Map<String, dynamic>>> get_orders() async{
  return await supabase.
  from("orders").
  select().
  eq("id_user", supabase.auth.currentUser!.id);
}
Future<List<Map<String, dynamic>>> getPoints() async{
  return await supabase.
  from("points").
  select();
}
