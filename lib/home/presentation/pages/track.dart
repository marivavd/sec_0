import 'package:flutter/material.dart';
import 'package:untitled6/home/data/repository/supabase.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class Track extends StatefulWidget {
  const Track({super.key});


  @override
  State<Track> createState() => _TrackState();
}

class _TrackState extends State<Track> {
  var mapController = null;
  List<Point> mapPoints = [];
  List<MapObject<dynamic>> mapObjects = [];
  Future<void> initMapObjects() async{
    mapPoints = await getPoints().then((value){
      return value.map((e) =>
          Point(
              latitude: double.parse(e["latitude"]),
              longitude: double.parse(e["longitude"]))).toList();
    });
    for (var val in mapPoints){
      mapObjects.add(
          PlacemarkMapObject(
              mapId: MapObjectId("point-${val.latitude}-${val.longitude}"),
              icon: PlacemarkIcon.single(PlacemarkIconStyle(
                scale: 5,
                  image: BitmapDescriptor.fromAssetImage("assets/track.png"),
                  anchor: const Offset(0.5, 1)
              )),
              point: val)
      );
    }
    mapObjects.add(
      PolygonMapObject(
          mapId: MapObjectId('polygon map object'),
          polygon: Polygon(
            outerRing: LinearRing(
              points: mapPoints,
            ),
            innerRings: const [],
          ),
        strokeColor: Color(0xFF0560FA),
        strokeWidth: 1,
        fillColor: Color(0xFF0560FA)
      )
    );
    await mapController?.moveCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
            target: mapPoints.first,
          zoom: 12
        )
      ),
    );
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: 320,
            child: YandexMap(
              onMapCreated: (controller) async{
                mapController = controller;
                if (mapPoints.isEmpty){
                  await initMapObjects();
                }
              },
              mapObjects: mapObjects,
            ),
          )
        ],
      ),
    );
  }
}
