import 'package:flutter/material.dart';

class Text_Field extends StatelessWidget{
  final String hint;
  final TextEditingController controller;
  const Text_Field({required this.hint, required this.controller});



  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: Color(0x26000026),
                  blurRadius: 5,
                  offset: Offset(0, 2)
              )
            ]
        ),
        child: SizedBox(
            height: 32,
            child: TextField(
              controller: controller,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.symmetric(vertical: 14, horizontal: 10),
                hintText: hint,
                hintStyle: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: Color(0xFFCFCFCF),
                    fontFamily: 'Roboto'
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(4),
                ),
              ),
            ))
    );
  }
}
