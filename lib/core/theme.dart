import 'package:flutter/material.dart';

var lightTheme = ThemeData(
  textTheme: TextTheme(
    titleLarge: TextStyle(
        fontSize: 24,
        fontWeight: FontWeight.w500,
        color: Color(0xFF3A3A3A),
        fontFamily: 'Roboto'
    ),
    titleMedium: TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.w500,
        color: Color(0xFFA7A7A7),
        fontFamily: 'Roboto'
    ),
    labelLarge: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.w500,
        color: Color(0xFFA7A7A7),
        fontFamily: 'Roboto'
    ),
  ),
    filledButtonTheme: FilledButtonThemeData(
        style: FilledButton.styleFrom(
            backgroundColor: Color.fromARGB(255, 5, 96, 250),
            disabledBackgroundColor: Color.fromARGB(255, 167, 167, 167),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(4)
            )
        )
    )
);